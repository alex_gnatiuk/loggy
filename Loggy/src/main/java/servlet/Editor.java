package servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet.utils.FileUpdater;
import servlet.utils.User;

/**
 * Servlet implementation of user edotor
 */
public class Editor extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FileUpdater updater;
	private User connectedUser;
	private static Logger log;

	static {
		log = Logger.getLogger(Editor.class.getName());
		try {
			LogManager.getLogManager().readConfiguration(
					Editor.class.getResourceAsStream("/logging.properties"));
		} catch (IOException e) {
			System.err.println("Could not setup logger configuration: " + e);
		}
	}

	public Editor() {
		super();
		updater = new FileUpdater();
		connectedUser = new User();
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			connectedUser = (User) request.getAttribute("user");
			request.setAttribute("succes", "OK");
			updater.edit(connectedUser);
			connectedUser = updater.getUser(connectedUser.getUserName());
			RequestDispatcher dispatcher = request
					.getRequestDispatcher("./Pages/personal.jsp");
			if (dispatcher != null) {
				dispatcher.forward(request, response);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "Failed to update the user : " + e);
		}
	}
}
