package servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import servlet.utils.FileUpdater;
import servlet.utils.User;

/**
 * Servlet implementation of user loginer
 */
public class Loginer extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FileUpdater updater;
	private User connectedUser;
	private static Logger log;

	static {
		log = Logger.getLogger(Loginer.class.getName());
		try {
			LogManager.getLogManager().readConfiguration(
					Loginer.class.getResourceAsStream("/logging.properties"));
		} catch (IOException e) {
			System.err.println("Could not setup logger configuration: " + e);
		}
	}

	public Loginer() {
		super();
		updater = new FileUpdater();
		connectedUser = new User();
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			String username = (String) request.getParameter("username");
			String password = (String) request.getParameter("password");
			connectedUser.setUserName(username);
			connectedUser.setPassword(password);
			RequestDispatcher dispatcher = null;
			if (updater.isRegistered(connectedUser)) {
				connectedUser = updater.getUser(username);
				request.setAttribute("user", connectedUser);
				dispatcher = request
						.getRequestDispatcher("./Pages/personal.jsp");
				if (dispatcher != null) {
					dispatcher.forward(request, response);
				}
			} else {
				connectedUser.setErrors("username", "Wrong password or login");
				request.setAttribute("user", connectedUser);
				dispatcher = request
						.getRequestDispatcher("./Pages/login.jsp");
				if (dispatcher != null) {
					dispatcher.forward(request, response);
				}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "Failed to finish login process the user : " + e);
		}
	}
	
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String username = (String) request.getParameter("username");
		String password = (String) request.getParameter("password");
		if(username == null && password == null) {
			response.sendRedirect("./Pages/login.jsp");
		}
	}
}
