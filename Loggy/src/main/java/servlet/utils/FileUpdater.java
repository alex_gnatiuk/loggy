package servlet.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import servlet.Registrator;

/**
 * implements Updater for work with files
 * 
 * @author Alex Gnatiuk
 * 
 */
public class FileUpdater implements Updater {
	private File file;
	private static Logger log;

	static {
		log = Logger.getLogger(FileUpdater.class.getName());
		try {
			LogManager.getLogManager().readConfiguration(
					FileUpdater.class.getResourceAsStream("/logging.properties"));
		} catch (IOException e) {
			System.err.println("Could not setup logger configuration: " + e);
		}
	}

	public FileUpdater() {
		File temp = new File(Registrator.class.getResource("").getPath()
				+ "Loginer.class");
		file = new File(temp.getParentFile().getParentFile().getParentFile()
				.getParentFile().toString()
				+ File.separatorChar
				+ "src"
				+ File.separatorChar
				+ "main"
				+ File.separatorChar
				+ "webapp"
				+ File.separatorChar
				+ "Data"
				+ File.separatorChar + "info.data");
	}

	/**
	 * appends new user to the end of file
	 */
	@Override
	public void append(User user) {
		try (PrintWriter out = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(file, true)))) {
			out.println();
			for (int i = 0; i < user.getUserName().length() + 4; i++) {
				out.print("#");
			}
			out.println();
			out.println("##" + user.getUserName() + "##");
			for (int i = 0; i < user.getUserName().length() + 4; i++) {
				out.print("#");
			}
			out.println();
			out.println("USERNAME = " + user.getUserName());
			out.println("PASSWORD = " + user.getPassword());
			out.println("FIRST_NAME = " + user.getFirstName());
			out.println("LAST_NAME = " + user.getLastName());
			out.println("E-MAIL = " + user.getEmail());
			out.println("PHONE = " + user.getPhone());
			out.flush();
		} catch (Exception e) {
			log.log(Level.SEVERE, "Failed to append the user : " + e);
		}
	}

	/**
	 * changes user parameters
	 */
	@Override
	public void edit(User user) {
		List<String> temp = new ArrayList<>();
		String str = "";
		try (Scanner s = new Scanner(file)) {
			while (s.hasNext()) {
				str = s.nextLine();
				if (str.equals("USERNAME = " + user.getUserName())) {
					temp.add("USERNAME = " + user.getUserName());
					str = s.nextLine();
					temp.add("PASSWORD = " + user.getPassword());
					str = s.nextLine();
					temp.add("FIRST_NAME = " + user.getFirstName());
					str = s.nextLine();
					temp.add("LAST_NAME = " + user.getLastName());
					str = s.nextLine();
					temp.add("E-MAIL = " + user.getEmail());
					str = s.nextLine();
					temp.add("PHONE = " + user.getPhone());
				} else {
					temp.add(str);
				}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "Failed to find the user : " + e);
		}
		clear();

		try (PrintWriter out = new PrintWriter(new OutputStreamWriter(
				new FileOutputStream(file, true)))) {
			for (String s : temp) {
				out.println(s);
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "Failed to write the file : " + e);
		}
	}
	
	private void clear() {
		try (FileWriter fstream1 = new FileWriter(file);
				BufferedWriter out1 = new BufferedWriter(fstream1)) {
			out1.write("");
		} catch (Exception e) {
			log.log(Level.SEVERE, "Failed to clear the file : " + e);
		}
	}

	/**
	 * check user existance
	 * @param user user to check
	 * @return true if user exists
	 */
	public boolean isExists(User user) {
		String name = "##" + user.getUserName() + "##";
		try (Scanner s = new Scanner(file)) {
			while (s.hasNext()) {
				if (name.equals(s.nextLine())) {
					return true;
				}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "Failed to check user existance : " + e);
		}
		return false;
	}

	/**
	 * check user registration
	 * @param user to check
	 * @return true if user is registered
	 */
	public boolean isRegistered(User user) {
		boolean isRegistered = false;
		String name = "USERNAME = " + user.getUserName();
		String password = "PASSWORD = " + user.getPassword();
		try (Scanner s = new Scanner(file)) {
			while (s.hasNext()) {
				if ((name.equals(s.nextLine()) && (password
						.equals(s.nextLine())))) {
					isRegistered = true;
					return isRegistered;
				}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "Failed to check user registration : " + e);
		}
		return isRegistered;
	}

	/**
	 * returns user bean by its username
	 * @param username to search
	 * @return null if nothing found, User object when found
	 */
	public User getUser(String username) {
		String name = "USERNAME = " + username;
		User user = new User();
		user.setUserName(username);
		try (Scanner s = new Scanner(file)) {
			while (s.hasNext()) {
				if (name.equals(s.nextLine())) {
					user.setPassword(s.nextLine().replace("PASSWORD = ", ""));
					user.setFirstName(s.nextLine().replace("FIRST_NAME = ", ""));
					user.setLastName(s.nextLine().replace("LAST_NAME = ", ""));
					user.setEmail(s.nextLine().replace("E-MAIL = ", ""));
					user.setPhone(s.nextLine().replace("PHONE = ", ""));
				}
			}
		} catch (Exception e) {
			log.log(Level.SEVERE, "Failed to get user by username : " + e);
		}
		return user;
	}
}