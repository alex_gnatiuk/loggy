package servlet.utils;

public interface Updater {
	public void append(User user);
	public void edit(User data);
}
