package servlet.utils;

import java.util.HashMap;

/**
 * 
 * @author Alex Gnatiuk
 * class implements sites user
 *
 */
public class User {
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String userName;
	private String password;
	private HashMap<String, String> errors;

	public User() {
		firstName = "";
		lastName = "";
		email = "";
		phone = "";
		userName = "";
		password = "";
		errors = new HashMap<String, String>();
	}

	/**
	 * valid check
	 * @return true if use is valid
	 */
	public boolean isValid() {
		boolean isSuccesfull = true;
		if (firstName.equals("")) {
			errors.put("firstName", "Please enter your first name");
			isSuccesfull = false;
		}
		if (lastName.equals("")) {
			errors.put("lastName", "Please enter your last name");
			isSuccesfull = false;
		}
		if (email.equals("")) {
			errors.put("email", "Please enter your e-mail");
			isSuccesfull = false;
		}
		if (phone.equals("")) {
			errors.put("phone", "Please enter your phone");
			isSuccesfull = false;
		}
		if (userName.equals("")) {
			errors.put("userName", "Please enter your username");
			isSuccesfull = false;
		}
		if (password.equals("")) {
			errors.put("password", "Please enter your password");
			isSuccesfull = false;
		}
		return isSuccesfull;
	}

	public String getErrorMsg(String s) {
		String errorMsg = (String) errors.get(s.trim());
		return (errorMsg == null) ? "" : errorMsg;
	}

	public void setErrors(String key, String msg) {
		errors.put(key, msg);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
