<jsp:useBean id="user" class="servlet.utils.User" scope="request">
	<jsp:setProperty name="user" property="*" />
	<jsp:setProperty name="user" property="succes" value = "false"/>
</jsp:useBean>

<%
	if (user.isValid()) {
%>
<jsp:forward page="/Editor" />
<%
	} else {
%>
<jsp:forward page="./personal.jsp" />
<%
	}
%>