<jsp:useBean id="user" class="servlet.utils.User" scope="request">
	<jsp:setProperty name="user" property="*" />
</jsp:useBean>

<%
	if (user.isValid()) {
%>
<jsp:forward page="/Loginer" />
<%
	} else {
%>
<jsp:forward page="./login.jsp" />
<%
	}
%>