<html>
<head>
<meta charset="UTF-8">
<title>Login Page</title>
<link rel="stylesheet" href="/Loggy/Pages/css/styles.css"
	type="text/css" />
</head>

<body bgcolor="#fdf5e6">
	<form action="/Loggy/Loginer" method="post">
		<table>
			<tr>
				<td colspan="2" align="center"><h3>
						<i>Login Page</i>
					</h3></td>
			</tr>
			<tr>
				<td align="right">Username</td>
				<td align="left"><input type="text" name="username"
					size="30" maxlength="20" /> <font size=2 color=red></font></td>
			</tr>
			
			<tr>
				<td align="right">Password</td>
				<td align="left"><input type="password" name="password"
					size="30" maxlength="20" /> <font size=2 color=red></font></td>
			</tr>
			
			<tr>
				<td align="center" colspan="2"><a href = "/Loggy/Pages/index.html">Registration Page</a></td>
			</tr>

			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="Login" /></td>
			</tr>
		</table>
	</form>
</body>

</html>