<jsp:useBean id="user" class="servlet.utils.User" scope="request">
	<jsp:setProperty name="user" property="*" />
</jsp:useBean>
<%
	if (user.getUserName().equals("")) {
%>
<jsp:forward page="./login.jsp" />
<%
	}
%>

<html>
<head>
<meta charset="UTF-8">
<title>Personal page</title>
<link rel="stylesheet" href="/Loggy/Pages/css/styles.css" type="text/css" />
</head>

<body bgcolor="#fdf5e6">
	<form action="/Loggy/Pages/processing.jsp" method="post" id="form">
		<table>
			<tr>
				<td colspan="2" align="center"><h3>
						<i>Personal Page</i>
					</h3></td>
			</tr>
			<tr>
				<td align="right">First Name</td>
				<td align="left"><input type="text" name="firstName" size="30"
					maxlength="20" value="<%=user.getFirstName()%>" /> <font size=2
					color=red><%=user.getErrorMsg("firstName")%></font></td>
			</tr>
			<tr>
				<td align="right">Last Name</td>
				<td align="left"><input type="text" name="lastName" size="30"
					maxlength="20" value="<%=user.getLastName()%>" /> <font size=2
					color=red><%=user.getErrorMsg("lastName")%></font></td>
			</tr>
			<tr>
				<td align="right">E-mail</td>
				<td align="left"><input type="text" name="email" size="30"
					maxlength="20" value="<%=user.getEmail()%>" /> <font size=2
					color=red><%=user.getErrorMsg("email")%></font></td>
			</tr>
			<tr>
				<td align="right">Phone</td>
				<td align="left"><input type="text" name="phone" size="30"
					maxlength="20" value="<%=user.getPhone()%>" /> <font size=2
					color=red><%=user.getErrorMsg("phone")%></font></td>
			</tr>
			<tr>
				<td><br></td>
			</tr>
			<tr>
				<td align="right">Username</td>
				<td align="left"><input type="text" name="userName" size="30"
					maxlength="20" value="<%=user.getUserName()%>" readonly /> <font
					size=2 color=red><%=user.getErrorMsg("userName")%></font></td>
			</tr>
			<tr>
				<td align="right">Password</td>
				<td align="left"><input type="text" name="password" size="30"
					maxlength="20" value="<%=user.getPassword()%>" /> <font size=2
					color=red><%=user.getErrorMsg("password")%></font></td>
			</tr>
			<tr>
				<td align = "center" colspan = "2"><font size=2 color=green><%if(request.getAttribute("succes")!=null)request.getAttribute("succes");%></font>
				</td>
			</tr>

			<tr>
				<td align="right"><input type="submit" value="Save" name="save" /></td>

				<td align="left"><input type="submit" value="Logout"
					name="logout" /></td>
			</tr>
		</table>
	</form>
</body>

</html>