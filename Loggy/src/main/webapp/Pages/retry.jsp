<jsp:useBean id="user" class="servlet.utils.User" scope="request" />

<html>
<head>
<meta charset="UTF-8">
<title>Registration</title>
<link rel="stylesheet" href="./css/styles.css" type="text/css" />
</head>

<body bgcolor="#fdf5e6">
	<form action="registration.jsp" method="post">
		<table>
			<tr>
				<td colspan="2" align="center"><h3>
						<i>Registration Page</i>
					</h3></td>
			</tr>
			<tr>
				<td align="right">First Name</td>
				<td align="left"><input type="text" name="firstName" size="30"
					maxlength="20" value="<%=user.getFirstName()%>" /> <font size=2
					color=red><%=user.getErrorMsg("firstName")%></font></td>
			</tr>
			<tr>
				<td align="right">Last Name</td>
				<td align="left"><input type="text" name="lastName" size="30"
					maxlength="20" value="<%=user.getLastName()%>" /> <font size=2
					color=red><%=user.getErrorMsg("lastName")%></font></td>
			</tr>
			<tr>
				<td align="right">E-mail</td>
				<td align="left"><input type="text" name="email" size="30"
					maxlength="20" value="<%=user.getEmail()%>" /> <font size=2
					color=red><%=user.getErrorMsg("email")%></font></td>
			</tr>
			<tr>
				<td align="right">Phone</td>
				<td align="left"><input type="text" name="phone" size="30"
					maxlength="20" value="<%=user.getPhone()%>" /> <font size=2
					color=red><%=user.getErrorMsg("phone")%></font></td>
			</tr>
			<tr>
				<td><br></td>
			</tr>
			<tr>
				<td align="right">Username</td>
				<td align="left"><input type="text" name="userName" size="30"
					maxlength="20" value="<%=user.getUserName()%>" /> <font size=2
					color=red><%=user.getErrorMsg("username")%></font></td>
			</tr>
			<tr>
				<td align="right">Password</td>
				<td align="left"><input type="password" name="password"
					size="30" maxlength="20" value="" /> <font size=2 color=red><%=user.getErrorMsg("password")%></font></td>
			</tr>
			
			<tr>
				<td align="center" colspan="2"><a href = "/Loggy/Pages/login.jsp">I already have an account</a></td>
			</tr>

			<tr>
				<td align="center" colspan="2"><input type="submit"
					value="Registration" /></td>
			</tr>
		</table>
	</form>
</body>

</html>